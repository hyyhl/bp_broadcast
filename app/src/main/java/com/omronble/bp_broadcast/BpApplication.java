package com.omronble.bp_broadcast;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.omron.lib.ohc.OHQDeviceManager;

import org.litepal.LitePalApplication;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.omronble.bp_broadcast.tools.Constants.ACTION_ALARM_REPLENISH_STOCK;
import static com.omronble.bp_broadcast.tools.Constants.ALARM_REPLENISH_STOCK_CODE;

public class BpApplication extends LitePalApplication {

    private static BpApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();
        SpeechUtility utility = SpeechUtility.createUtility(this, SpeechConstant.APPID +"=5e70d080");
        Log.d("----" , "utility is null :" + (utility == null) );
        OHQDeviceManager.init(this);
        instance = this;
        startAlarm();
    }

    public static BpApplication getInstance() {
        return instance;
    }

    /**
     * 启动定时器（使用系统闹铃服务）00:00触发
     */
    private void startAlarm() {
      /*  setAlarm(AlarmManager.RTC_WAKEUP, 0, 0,
                AlarmManager.INTERVAL_DAY, ALARM_REPLENISH_STOCK_CODE, ACTION_ALARM_REPLENISH_STOCK);*/
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH ,1);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        Log.d(getClass().getSimpleName(),"startAlarm>>>" + sdf.format(calendar.getTime())  );
        long startTime = calendar.getTimeInMillis();//表示闹钟第一次执行时间，
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, startTime, AlarmManager.INTERVAL_DAY, PendingIntent.getBroadcast(this,
                ALARM_REPLENISH_STOCK_CODE, new Intent(ACTION_ALARM_REPLENISH_STOCK), PendingIntent.FLAG_UPDATE_CURRENT));
    }

    /**
     * 取消闹钟
     */
    private void cancelAlarm(int requestCode,String action) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(
                PendingIntent.getBroadcast(this, requestCode, new Intent(action), PendingIntent.FLAG_UPDATE_CURRENT));
    }

    private void setAlarm(int type, int hour,int minute, long intervalMillis, int requestCode, String action) {
        Calendar now = Calendar.getInstance();
        Calendar targetTime = (Calendar) now.clone();
        targetTime.set(Calendar.HOUR_OF_DAY, hour);
        targetTime.set(Calendar.MINUTE, minute);
        targetTime.set(Calendar.SECOND, 0);
        targetTime.set(Calendar.MILLISECOND, 0);
        if (targetTime.before(now))
            targetTime.add(Calendar.DATE, 1);
        // 方便测试，这里指定即时启动，每20秒执行一次
        setAlarm(type, 0, 24 * 60 * 60 * 1000, requestCode, action);
//        setAlarm(type, targetTime.timeInMillis, intervalMillis, requestCode, action)
    }

    /**
     * 设置闹钟
     */
    private void setAlarm(int type, long triggerAtMillis, long intervalMillis, int requestCode,String action) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(type, triggerAtMillis, intervalMillis, PendingIntent.getBroadcast(this,
                requestCode, new Intent(action), PendingIntent.FLAG_UPDATE_CURRENT));
    }


}
