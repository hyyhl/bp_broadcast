package com.omronble.bp_broadcast.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.huawei.hms.framework.common.StringUtils;
import com.huawei.hms.utils.StringUtil;
import com.omron.lib.OMRONLib;
import com.omron.lib.model.BPData;
import com.omronble.bp_broadcast.tools.Constants;
import com.omronble.bp_broadcast.R;
import com.omronble.bp_broadcast.tools.Utils;
import com.omronble.bp_broadcast.bean.XBpData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.litepal.crud.DataSupport;

import java.text.SimpleDateFormat;
import java.util.List;

public class SysFragment extends DialogFragment {

    private TextView tvAccountId,
            tvVersion,
            tvDevice,tvDeviceStatus,
            tvNetwork,tvNetworkStatus,
            tvNewestData,
            tvTime,
            tvData,
            btnSys;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public SysFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = this.getDialog().getWindow();
        //去掉dialog默认的padding
        int padding = (int) (getResources().getDisplayMetrics().density * 10);
        window.getDecorView().setPadding(padding, 0, padding, 0);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.dimAmount = 0.0f;
        window.setAttributes(lp);
        setCancelable(false);
        EventBus.getDefault().register(this);
        return inflater.inflate(R.layout.fragment_sys, container, false);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvAccountId = view.findViewById(R.id.tvAccountId);
        tvVersion = view.findViewById(R.id.tvVersion);
        tvDevice = view.findViewById(R.id.tvDevice);
        tvDeviceStatus = view.findViewById(R.id.tvDeviceStatus);
        tvNetwork = view.findViewById(R.id.tvNetwork);
        tvNetworkStatus = view.findViewById(R.id.tvNetworkStatus);
        tvNewestData = view.findViewById(R.id.tvNewestData);
        tvTime = view.findViewById(R.id.tvTime);
        tvData = view.findViewById(R.id.tvData);
        btnSys = view.findViewById(R.id.btnSys);
        btnSys.setOnClickListener(view1 -> dismiss());
    }

    @Override
    public void onResume() {
        super.onResume();
//        List list = OMRONLib.getInstance().getOMRONDeviceList();
//        Log.d("----------------",""+list.size());

        SharedPreferences  mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String deviceId = mSharedPreferences.getString(Constants.deviceId,null);
        String deviceMac = mSharedPreferences.getString(Constants.deviceMac,null);
        tvAccountId.setText( "账户ID：" + deviceId);
        tvVersion.setText( "系统版本：v" + Utils.getAppVersionName(getContext()) );

        tvDevice.setText("设备：" + ((null != deviceId && null != deviceMac) ? Constants.mDeviceNameValue : "" ));
        tvDeviceStatus.setText( (null != deviceId && null != deviceMac) ? "链接" : "断开");
        tvDeviceStatus.setTextColor(ContextCompat.getColor(getContext(),(null != deviceId && null != deviceMac)?R.color.green_x:android.R.color.holo_red_light));

        String netType = Utils.getNetworkName(getActivity());
        tvNetwork.setText( "网络：" + netType);
        tvNetworkStatus.setText( (null != netType && !netType.equals("")) ? "链接" : "断开");
        tvNetworkStatus.setTextColor( ContextCompat.getColor(getContext(),Utils.isNetworkConnected(getContext())?R.color.green_x:android.R.color.holo_red_light) );

        tvNewestData.setText( (null != netType && !netType.equals("")) ? "已上传" : "待上传");

        tvTime.setText( "时间：" );
        List<XBpData> xlist = DataSupport.findAll(XBpData.class);
        if ( xlist.size() > 0 ) {
            tvTime.append( sdf.format(xlist.get(0).getMeasureTime()) );
            tvData.append( "高压：" + xlist.get(0).getSystolic() +
                    "\n低压：" + xlist.get(0).getDiastolic() +
                    "\n脉搏：" + xlist.get(0).getPulse() );
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BPData event) {
        if ( event != null ) {
            tvTime.setText( "时间：" );
            tvTime.append( sdf.format(event.getMeasureTime()) );
            tvData.setText( "高压：" + event.getSystolic() +
                    "\n低压：" + event.getDiastolic() +
                    "\n脉搏：" + event.getPulse() );
        }
    }

}
