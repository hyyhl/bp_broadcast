package com.omronble.bp_broadcast.fragment;

import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.omronble.bp_broadcast.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlueToothFragment extends DialogFragment {

    private TextView tvBlue;
    private Switch switchBlue;
    private Button btnBlue;
    private BluetoothAdapter mBluetoothAdapter;
    public BlueToothFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = this.getDialog().getWindow();
        //去掉dialog默认的padding
        int padding = (int) (getResources().getDisplayMetrics().density * 10);
        window.getDecorView().setPadding(padding, 0, padding, 0);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.dimAmount = 0.0f;
        window.setAttributes(lp);
        setCancelable(false);
        return inflater.inflate(R.layout.fragment_blue_tooth, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        tvBlue = view.findViewById(R.id.tvBlue);
        switchBlue = view.findViewById(R.id.switchBlue);
        btnBlue = view.findViewById(R.id.btnBlue);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        switchBlue.setChecked( mBluetoothAdapter != null && mBluetoothAdapter.isEnabled() );
        tvBlue.setText( switchBlue.isChecked() ? "蓝牙:开" : "蓝牙:关" );

        switchBlue.setOnCheckedChangeListener((compoundButton, b) -> {
            if ( !switchBlue.isPressed() )
                return;

            if ( null != mBluetoothAdapter ) {
                if (switchBlue.isChecked())
                    mBluetoothAdapter.enable();
                else
                    mBluetoothAdapter.disable();
            } else {
                Toast.makeText(getActivity(), "获取蓝牙权限失败，无法修改蓝牙状态!", Toast.LENGTH_SHORT).show();
                switchBlue.setChecked( mBluetoothAdapter != null && mBluetoothAdapter.isEnabled() );
            }
            tvBlue.setText( switchBlue.isChecked() ? "蓝牙:开" : "蓝牙:关" );

        });
        btnBlue.setOnClickListener(view1 -> dismiss());
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        switchBlue.setChecked( mBluetoothAdapter != null && mBluetoothAdapter.isEnabled() );
        tvBlue.setText( switchBlue.isChecked() ? "蓝牙:开" : "蓝牙:关" );
    }
}
