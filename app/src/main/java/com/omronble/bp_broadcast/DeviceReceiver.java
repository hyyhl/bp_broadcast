package com.omronble.bp_broadcast;

import android.app.admin.DeviceAdminReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.huawei.android.app.admin.DeviceApplicationManager;
import com.huawei.android.app.admin.DeviceControlManager;
import com.omronble.bp_broadcast.tools.DeviceMethod;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DeviceReceiver extends DeviceAdminReceiver {

    @Override
    public void onEnabled(@NonNull Context context, @NonNull Intent intent) {
        DeviceMethod.getInstance(context).setDefaultLauncher();
        DeviceMethod.getInstance(context).addPersistentApp();
        DeviceMethod.getInstance(context).setSystemUpdateDisabled(true);
    }

    @Override
    public void onDisabled(@NonNull Context context, @NonNull Intent intent) {

    }

    @Nullable
    @Override
    public CharSequence onDisableRequested(@NonNull Context context, @NonNull Intent intent) {
        return context.getString(R.string.disable_warning);
    }
}
