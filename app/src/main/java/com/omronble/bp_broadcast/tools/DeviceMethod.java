package com.omronble.bp_broadcast.tools;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.huawei.android.app.admin.DeviceApplicationManager;
import com.huawei.android.app.admin.DeviceControlManager;
import com.huawei.android.app.admin.DevicePackageManager;
import com.huawei.android.app.admin.DeviceRestrictionManager;
import com.omronble.bp_broadcast.BpApplication;
import com.omronble.bp_broadcast.DeviceReceiver;

import java.util.ArrayList;

public class DeviceMethod {
    private static DeviceMethod mDeviceMethod;

    private DevicePolicyManager devicePolicyManager;
    private ComponentName componentName;
    private Context mContext;

    private DeviceMethod (Context context){
        mContext=context;
        //获取设备管理服务
        devicePolicyManager=(DevicePolicyManager) BpApplication.getInstance()
                .getSystemService(Context.DEVICE_POLICY_SERVICE);
        //DeviceReceiver 继承自 DeviceAdminReceiver
        componentName=new ComponentName(BpApplication.getInstance(), DeviceReceiver.class);
    }

    public static DeviceMethod getInstance(Context context){
        if (mDeviceMethod==null) {
            synchronized (DeviceMethod.class) {
                if (mDeviceMethod == null) {
                    mDeviceMethod = new DeviceMethod(context);
                }
            }
        }
        return mDeviceMethod;
    }

    public boolean isActivated(){
        boolean activate = devicePolicyManager.isAdminActive(componentName);
        Log.e(getClass().getSimpleName(),"isActivated : " + activate );
        return activate;
    }

    // 激活程序
    public void onActivate() {
        //判断是否激活  如果没有就启动激活设备
        if (!devicePolicyManager.isAdminActive(componentName)) {
            Intent intent = new Intent(
                    DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                    componentName);
//            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "提示文字");
            mContext.startActivity(intent);
        } else
            Log.d("","device is activated..");
    }

    /**
     * 移除程序 如果不移除程序 APP无法被卸载
     */
    public void onRemoveActivate() {
        devicePolicyManager.removeActiveAdmin(componentName);
    }

    public void setDefaultLauncher() {
        DeviceControlManager deviceControlManager = new DeviceControlManager();
        String pkg = mContext.getPackageName();
        String cls = "com.omronble.bp_broadcast.activity.MainActivity";
        try {
            deviceControlManager.setDefaultLauncher(componentName,pkg,cls);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(),"setDefaultLauncher exception : " + e.getMessage() );
        }
    }

    public void setSystemUpdateDisabled(boolean disabled) {
        DeviceRestrictionManager manager = new DeviceRestrictionManager();
        try {
            boolean result = manager.setSystemUpdateDisabled(componentName,disabled);
            Log.d( getClass().getSimpleName(),"setSystemUpdateDisabled result : " + result );
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(),"setSystemUpdateDisabled exception : " + e.getMessage() );
        }
    }


     public void addPersistentApp() {
        DeviceApplicationManager deviceApplicationManager = new DeviceApplicationManager();
        ArrayList<String> list = new ArrayList<>();
        String cls0 = "com.omronble.bp_broadcast.activity.MainActivity";
        String cls1 = "com.omronble.bp_broadcast.XService";
        list.add(cls0);
        list.add(cls1);
        try {
            deviceApplicationManager.addPersistentApp(componentName,list);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(),"addPersistentApp exception : " + e.getMessage() );
        }
    }


    public void installPackage(String packagePath) {
        DevicePackageManager devicePackageManager = new DevicePackageManager();
        try {
            devicePackageManager.installPackage(componentName,packagePath);
        } catch (Exception e) {
            Log.e( getClass().getSimpleName() , "installPackage exception : " + e.getMessage() + "-" + isActivated() );
        }
    }

}
