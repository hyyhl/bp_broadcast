package com.omronble.bp_broadcast.tools;

import com.omron.lib.device.DeviceType;

public class Constants {
    public static final DeviceType mDeviceType = DeviceType.BLOOD_9200;
    public static final String mDeviceNameValue = "HEM-9200";
    public static final String deviceId = "device_id";
    public static final String deviceName = "device_name";
    public static final String deviceMac = "device_mac";

    /** 声音滑动是否可用 */
    public static final String VOICE_ENABLE = "voice_enable";

    public static final String CITY = "city";
    public static final String CITY_ID = "city_id";

    public static final String ACTION_ALARM_REPLENISH_STOCK = "witmoon.auto.replenish.stock.action";
    public static final int ALARM_REPLENISH_STOCK_CODE = 11;


//    public static final String UPDATE_APK = "http://tangfan.top/api/getcurversion";
    public static final String UPDATE_APK = "http://odm-app-stg.omronhealthcare.com.cn/api/getcurversion";

//    public static final String UPLOAD = "http://tangfan.top/api/Az/Bp";
    public static final String UPLOAD = "http://odm-stg.omronhealthcare.com.cn/api/v1/bp";

    public static final String MD5KEY = "123456";

    public static final String PASS = "*#7370#";

}
