package com.omronble.bp_broadcast.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jaeger.library.StatusBarUtil;
import com.omronble.bp_broadcast.R;

public class Guide2Activity extends AppCompatActivity {

    private Button btnGuide2Next;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide2);
        StatusBarUtil.setColor(this, Color.WHITE);
        btnGuide2Next = findViewById(R.id.btnGuide2Next);
        btnGuide2Next.setOnClickListener(view -> {
            startActivity(new Intent(Guide2Activity.this,Guide3Activity.class));
            finish();
        });
    }
}
