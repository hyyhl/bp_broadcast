package com.omronble.bp_broadcast.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.omronble.bp_broadcast.tools.Constants;
import com.omronble.bp_broadcast.R;
import com.omronble.bp_broadcast.fragment.DevFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DevActivity extends BaseActivity {

    @BindView(R.id.tvBindStatus)
    TextView tvBindStatus;
    @BindView(R.id.tvBindName)
    TextView tvBindName;
    @BindView(R.id.btnBind)
    Button btnBind;
    @BindView(R.id.iBtnHome)
    ImageButton iBtnHome;
    private DevFragment fragment;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        StatusBarUtil.setColor(this, Color.BLACK);
        setContentView(R.layout.activity_dev);
        ButterKnife.bind(this);
        inits();
        handler = new Handler();
    }

    @Override
    protected void onResume() {

        SharedPreferences  mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String deviceId = mSharedPreferences.getString(Constants.deviceId,null);
        String deviceMac = mSharedPreferences.getString(Constants.deviceMac,null);
        tvBindName.setText("设备：" + ((null != deviceId && null != deviceMac) ? Constants.mDeviceNameValue : "" ));
        tvBindStatus.setText((null != deviceId && null != deviceMac) ? "已连接" : "未连接");

        super.onResume();
    }

    private void inits() {

        iBtnHome.setOnClickListener(view -> finish());
        btnBind.setOnClickListener(view -> {
            LinearLayout layout = (LinearLayout) LayoutInflater.from(DevActivity.this).inflate(R.layout.view_edit,null);
            final EditText edit = layout.findViewById(R.id.editPass);
            AlertDialog.Builder builder = new AlertDialog.Builder(DevActivity.this);
            builder.setTitle("提示");
            builder.setMessage("确定要重新绑定血压仪设备嘛？在完成重新绑定之前系统功能可能无法使用！");
            builder.setView( layout );
            builder.setPositiveButton("是", (dialog, which) -> {
                hideKeyboard();
                String pass = edit.getText().toString();
                if ( Constants.PASS.equals( pass ) ) {
                    handler.postDelayed(() -> {
                        fragment = new DevFragment();
                        fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
                    },150);
                } else {
                    Toast.makeText(DevActivity.this, "密码错误！", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            });
            builder.setNegativeButton("否",null);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            Button btnPos = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE); //解决页面按钮显示空白的问题
            Button btnNeg = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            btnPos.setTextColor(Color.RED);
            btnNeg.setTextColor(Color.RED);
        });
    }

    /**
     * 隐藏软键盘
     */
    public void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.e("hideKeyboard", e.toString());
        }
    }

}
