package com.omronble.bp_broadcast.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jaeger.library.StatusBarUtil;
import com.omronble.bp_broadcast.R;

public class GuideActivity extends AppCompatActivity {

    private Button btnUse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        StatusBarUtil.setColor(this, Color.WHITE);
        btnUse = findViewById(R.id.btnUse);
        btnUse.setOnClickListener(view -> {
            startActivity(new Intent(GuideActivity.this,Guide2Activity.class));
            finish();
        });
    }

    @Override
    public void onBackPressed() {

    }
}
