package com.omronble.bp_broadcast.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.omron.lib.OMRONLib;
import com.omron.lib.common.OMRONBLEErrMsg;
import com.omron.lib.device.DeviceType;
import com.omron.lib.model.BPData;
import com.omronble.bp_broadcast.tools.Constants;
import com.omronble.bp_broadcast.R;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;

public class Guide4Activity extends AppCompatActivity {

    private LinearLayout llGuide4A, llGuide4B;
    private ImageView loading, ivGuideB;
    private TextView tvGuide4;
    private Button btnGuide4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide4);
        StatusBarUtil.setColor(this, Color.WHITE);
        llGuide4A = findViewById(R.id.llGuide4A);
        llGuide4B = findViewById(R.id.llGuide4B);
        ivGuideB = findViewById(R.id.ivGuideB);
        loading = findViewById(R.id.loading);
        tvGuide4 = findViewById(R.id.tvGuide4);
        btnGuide4 = findViewById(R.id.btnGuide4);
        init();
    }

    private void init() {
        startAnimation();
        initPermission();
    }

    private void startAnimation() {
        Animation rotateAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate_anim);
        LinearInterpolator lin = new LinearInterpolator();
        rotateAnimation.setInterpolator(lin);
        loading.startAnimation(rotateAnimation);
    }

    private void stopAnimation() {
        Animation animation = loading.getAnimation();
        if (animation != null)
            animation.cancel();
        llGuide4A.setVisibility(View.GONE);
        llGuide4B.setVisibility(View.VISIBLE);
    }

    /**
     * 判断是否有SDK需要的权限，没有的话动态获取
     */
    private void initPermission() {
        RxPermissions permissions = new RxPermissions(this);
        permissions.setLogging(true);
        permissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        //权限已经都通过了，可以将程序继续打开了
                        OMRONLib omronlib = OMRONLib.getInstance();
                        DeviceType deviceType = Constants.mDeviceType;
                        omronlib.bindDevice(Guide4Activity.this, deviceType, myOmronBleCallBack);
                    } else {
                        loadFailure("获取权限失败");
                    }
                });
    }

    private void loadFailure(String msg) {
        stopAnimation();
        ivGuideB.setImageDrawable(ContextCompat.getDrawable(Guide4Activity.this, R.mipmap.fail));
        tvGuide4.setText(msg);
        btnGuide4.setText("重试");
        btnGuide4.setOnClickListener(view -> {
            finish();
        });
    }

    /*********************************欧姆龙*************************************/
    private OMRONLib.OmronBleCallBack myOmronBleCallBack = new OMRONLib.OmronBleCallBack() {
        @Override
        public void onFailure(OMRONBLEErrMsg omronbleErrMsg) {
            OMRONLib.getInstance().StopScan();
            runOnUiThread(() -> {
               /* SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(Guide4Activity.this);
                mSharedPreferences.edit()
                        .putString(Constants.deviceId,"HEM-9200T20180400128L")
                        .putString(Constants.deviceName,"BLEsmart_00000116B0495F045A76")
                        .apply();*/
                loadFailure(null != omronbleErrMsg ? omronbleErrMsg.getErrMsg() : "设备绑定失败");
            });

        }

        @Override
        public void onBindComplete(String deviceName, String deviceId, String mac) {
            OMRONLib.getInstance().StopScan();
            runOnUiThread(() -> {
                Toast.makeText(Guide4Activity.this, "设备绑定成功", Toast.LENGTH_SHORT).show();
                /* BLEsmart_00000116B0495F045A76,HEM-9200T20180400128L */
                SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(Guide4Activity.this);
                mSharedPreferences.edit()
                        .putString(Constants.deviceId, deviceId)
                        .putString(Constants.deviceName, deviceName)
                        .putString(Constants.deviceMac, mac)
                        .apply();
                /** 进入首页，关闭之前所有 */
                stopAnimation();
                btnGuide4.setOnClickListener(view -> {
                    Intent intent = new Intent(Guide4Activity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                });
            });

        }

        @Override
        public void onDataReadComplete(List<BPData> list) {

        }
    };


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
