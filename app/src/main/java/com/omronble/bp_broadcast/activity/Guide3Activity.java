package com.omronble.bp_broadcast.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;

import com.jaeger.library.StatusBarUtil;
import com.omronble.bp_broadcast.R;

public class Guide3Activity extends AppCompatActivity {

    private Button btnGuide3Next;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide3);
        StatusBarUtil.setColor(this, Color.WHITE);
        btnGuide3Next = findViewById(R.id.btnGuide3Next);
        btnGuide3Next.setOnClickListener(view -> {
            startActivity(new Intent(Guide3Activity.this,Guide4Activity.class));
            finish();
//                initPermission();
        });
    }




}
