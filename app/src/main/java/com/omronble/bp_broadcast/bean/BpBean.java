package com.omronble.bp_broadcast.bean;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

public class BpBean extends DataSupport implements Serializable {
    private String device_ble_cmn_id;
    private int sbp;
    private int dbp;
    private int pulse;
    private float lng;
    private float lat;
    private String measure_at;
    private String signature;

    public String getDevice_ble_cmn_id() {
        return device_ble_cmn_id;
    }

    public void setDevice_ble_cmn_id(String device_ble_cmn_id) {
        this.device_ble_cmn_id = device_ble_cmn_id;
    }

    public int getSbp() {
        return sbp;
    }

    public void setSbp(int sbp) {
        this.sbp = sbp;
    }

    public int getDbp() {
        return dbp;
    }

    public void setDbp(int dbp) {
        this.dbp = dbp;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public String getMeasure_at() {
        return measure_at;
    }

    public void setMeasure_at(String measure_at) {
        this.measure_at = measure_at;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
