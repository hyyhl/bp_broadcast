package com.omronble.bp_broadcast.bean;

import java.io.Serializable;

public class UpdateBean implements Serializable {
    /**
     * {
     *     "clientVersion": "1.0.1",
     *     "clientVersionCode": 1,
     *     "downloadUrl": "http://tangfan.top/file/775910492.apk",
     *     "description": "",
     *     "updateType": "must"
     * }
     */
    private String clientVersion;
    private int clientVersionCode;
    private String downloadUrl;
    private String description;
    private String updateType;

    public String getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public int getClientVersionCode() {
        return clientVersionCode;
    }

    public void setClientVersionCode(int clientVersionCode) {
        this.clientVersionCode = clientVersionCode;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }
}
