package com.omronble.bp_broadcast.bean;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

public class XBpData extends DataSupport implements Serializable {
    private static final String TAG = "XBpData";
    private int systolic;
    private int diastolic;
    private int pulse;
    private int arrhythmiaFlg;
    private int bmFlg;
    private int cwsFlg;
    private int measureUser;
    private long measureTime;

    public XBpData() {
    }

    public int getSystolic() {
        return this.systolic;
    }

    public void setSystolic(int var1) {
        this.systolic = var1;
    }

    public int getDiastolic() {
        return this.diastolic;
    }

    public void setDiastolic(int var1) {
        this.diastolic = var1;
    }

    public int getPulse() {
        return this.pulse;
    }

    public void setPulse(int var1) {
        this.pulse = var1;
    }

    public int getArrhythmiaFlg() {
        return this.arrhythmiaFlg;
    }

    public void setArrhythmiaFlg(int var1) {
        this.arrhythmiaFlg = var1;
    }

    public int getBmFlg() {
        return this.bmFlg;
    }

    public void setBmFlg(int var1) {
        this.bmFlg = var1;
    }

    public int getCwsFlg() {
        return this.cwsFlg;
    }

    public void setCwsFlg(int var1) {
        this.cwsFlg = var1;
    }

    public int getMeasureUser() {
        return this.measureUser;
    }

    public void setMeasureUser(int var1) {
        this.measureUser = var1;
    }

    public long getMeasureTime() {
        return this.measureTime;
    }

    public void setMeasureTime(long var1) {
        this.measureTime = var1;
    }

    public static String getTag() {
        return "XBpData";
    }
}
